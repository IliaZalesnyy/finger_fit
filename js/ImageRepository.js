import {Image} from '/js/Image.js';
export class ImageRepository {
  constructor() {
    this.imageArray = [];
    this.cacheLength = 243;
    this.loadImagesFromDisk();
  }

  loadImagesFromDisk(){
    return new Promise((resolve, reject) => {
      let lastId = -1;
      let counter = 0;
      for (let i = 0; i < this.cacheLength; i++) { 
        fetch(`/img/fingers/lvl1/${i}.png`)
        .then(response => response.blob())
        .then(imageBlob => {
            lastId = 2 + +lastId;
            this.imageArray.push(new Image(i, imageBlob, URL.createObjectURL(imageBlob), true));
            this.imageArray.push(new Image(i, imageBlob, URL.createObjectURL(imageBlob), false));
            if(counter + 1 === this.cacheLength){

              resolve(this.imageArray[this.currentIndex])
            }else{
              counter += 1;
            }
        }).catch(function() {
            resolve("null");
        });
      }
      
    });
  }
}