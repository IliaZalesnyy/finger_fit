import {Profile} from '/js/Profile.js';
import {Result} from '/js/Result.js'

export class ResultRepository {
  constructor() {
    this.profileArray = [];
    this.cacheLength = 32;
    this.storage = window.localStorage;
    this.loadLocalStorage();
  }

  loadLocalStorage(){
    var retrievedArray = this.storage.getItem('profileArray');
    if(retrievedArray != null){
      JSON.parse(retrievedArray).forEach(item => {
        var temp = new Profile(0, "", "");
        temp.fromStorage(item);
        this.profileArray.push(temp);
      })
    }
  }

  addProfile(name, age, note){
    this.profileArray.push(new Profile(name, age, note))
  }

  addResult(name, imageNum, timeSpent, solved, rightHand){
    this.profileArray.forEach(item => {
      if(item.name === name){
        item.results.push(new Result(imageNum, timeSpent, solved, rightHand))
      }
    })
  }

  getResults(name){
    return new Promise((resolve, reject) => {
      this.profileArray.forEach(item => {
        if(item.name === name){
          resolve(item);
        }
      })
      reject("null")
    })
    
  }

  saveData(){
    this.storage.setItem('profileArray', JSON.stringify(this.profileArray))
  }
}