import {Image} from '/js/Image.js';
import {ImageRepository} from '/js/ImageRepository.js'
import {ResultRepository} from '/js/ResultRepository.js'

export class ImageProvider {
  constructor(){
    this.currentIndex = -1;
    this.imageRepository = new ImageRepository();
    this.resultRepository = new ResultRepository();
    this.testingArray = [];

    this.currentTestingName = "";

    this.currentSelectedName = "";

    this.testLength = 64;
  }

  loadAllImages(){
    return this.imageRepository.loadImagesFromDisk();
  }

  getCurrentImage(){
    return new Promise((resolve, reject) => {
        resolve(this.imageRepository.imageArray[this.currentIndex]);
    });
  }

  setCurrentSolved(timeSpent, isSolved){
    if(this.currentIndex >= 0){
      this.imageRepository.imageArray[this.currentIndex].solved = isSolved;
      this.imageRepository.imageArray[this.currentIndex].timeSpent = timeSpent;
      this.addResult(timeSpent, isSolved);
    }
  }

  startTesting(name, age, note, testType, testNumArray){
    this.currentIndex = -1;
    this.currentTestingName = name + " " + Math.floor(Math.random() * 10000);
    this.resultRepository.addProfile(this.currentTestingName, age, note);
    if(testType == 0){
      this.testingArray = this.shuffle(this.imageRepository.imageArray);
    }else if(testType == 1){
      this.testingArray = this.arrayToTest(this.imageRepository.imageArray, testNumArray);
    }

  }

  endTesting(){
    this.resultRepository.saveData();
    return new Promise((resolve,reject) => {
      resolve(this.resultRepository.getResults(this.currentTestingName));
    });
  }

  getNextImage(){
    return new Promise((resolve, reject) => {
      if(this.testingArray.length == 0){
        this.loadAllImages()
        .then(imageArray => {
          this.currentIndex += 1;
          this.testingArray = this.shuffle(this.imageRepository.imageArray);
          if(this.currentIndex >= this.testingArray.length){

          }else{
            resolve(this.imageRepository.imageArray[this.currentIndex]);
          }
        })
      }else{
        this.currentIndex += 1;
        if(this.currentIndex >= this.testingArray.length || this.currentIndex >= this.testLength){
          reject('null');
        }else{
          resolve(this.testingArray[this.currentIndex]);
        }
      }
    });
  }


  getPreviousImage(){
    return new Promise((resolve, reject) => {
      this.currentIndex -= 1;
      if(this.currentIndex >= 0 && this.currentIndex < this.imageRepository.imageArray.length){
        resolve(this.imageRepository.imageArray[this.currentIndex]);
      }else if(this.imageRepository.imageArray.length > 0){
        this.currentIndex = 0;
        reject('edgeOfArray');
      }else{
        this.currentIndex = 0;
        resolve("null")
      }
    });
  }


  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
  }

  addResult(timeSpent, solved){
    return new Promise((resolve, reject) => {
      try{
        this.resultRepository.addResult(this.currentTestingName, this.testingArray[this.currentIndex].id, timeSpent, solved, this.testingArray[this.currentIndex].mirror);
      }catch(e){
        console.log(e)
      }finally{
        resolve(true)
      }
      
    })
  }

  loadResultList(){
    return new Promise((resolve, reject) => {
      resolve(this.resultRepository.profileArray)
    })
  }

  loadResult(name){
    return new Promise((resolve, reject) => {
      let tmp = this.resultRepository.getResults(name);
      if(tmp){
        resolve(tmp)
      }else{
        reject("null")
      }
     
    });
  }

  shuffle(array) {
    let currentIndex = array.length,  randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex != 0) {

      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }

    return array;
  }

  arrayToTest(array, numArray) {
    let arr = [];
    // While there remain elements to shuffle.
    numArray.forEach(num => {
      array.filter(item => item.id == num).forEach(item2 => {
        arr.push(item2)
      })
    })

    return arr;
  }

  blobToBase64(img) {
    return new Promise((resolve, reject) => {
      if(img.base64String != ''){
        resolve(img.base64String)
      }else{
        const reader = new FileReader();
        reader.onloadend = () => resolve(reader.result);
        reader.readAsDataURL(img.blob)
      }
      
    });
  }
}