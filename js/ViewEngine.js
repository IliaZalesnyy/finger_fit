import {TemplateCreator} from '/js/TemplateCreator.js'

export class ViewEngine {
  constructor() {
    this.loadingMockup = document.querySelector('.image-view__loading-mockup');
    this.loadingActive = document.querySelector('.image-view__loading-active');
    this.loadingError = document.querySelector('.image-view__loading-error');
    this.mainMenu = document.querySelector('.image-view__main-menu');
    this.resultMenu = document.querySelector('.image-view__results-menu');
    this.imageView = document.querySelector('.image-view__content-img');
    this.allResultsMenu = document.querySelector('.image-view__results-list-menu');
    this.profileList = document.querySelector('.results-list-menu__profiles');
    this.resList = document.querySelector('.results');

    this.currentResult = null;

    this.templateCreator = new TemplateCreator();
  }

  setLoading(state){
    var self = this;
    if(state === "loading"){
      this.loadingMockup.style.visibility = "visible";
      this.loadingMockup.style.opacity = "75%";
      this.loadingActive.style.visibility = "visible";
      this.resultMenu.style.visibility = "hidden";
      this.mainMenu.style.visibility = "hidden";
      this.allResultsMenu.style.visibility = "hidden";
      this.loadingError.style.visibility = "hidden";
    }else if(state === "allResultsMenu"){
      this.loadingMockup.style.visibility = "visible";
      this.loadingMockup.style.opacity = "95%";
      this.resultMenu.style.visibility = "hidden";
      this.mainMenu.style.visibility = "hidden";
      this.allResultsMenu.style.visibility = "visible";
      this.loadingActive.style.visibility = "hidden";
      this.loadingError.style.visibility = "hidden";
    }else if(state === "mainMenu"){
      this.loadingMockup.style.visibility = "visible";
      this.loadingMockup.style.opacity = "95%";
      this.resultMenu.style.visibility = "hidden";
      this.mainMenu.style.visibility = "visible";
      this.allResultsMenu.style.visibility = "hidden";
      this.loadingActive.style.visibility = "hidden";
      this.loadingError.style.visibility = "hidden";
    }else if(state === "resultMenu"){
      this.loadingMockup.style.visibility = "visible";
      this.loadingMockup.style.opacity = "95%";
      this.loadingActive.style.visibility = "hidden";
      this.mainMenu.style.visibility = "hidden";
      this.resultMenu.style.visibility = "visible";
      this.allResultsMenu.style.visibility = "hidden";
      this.loadingError.style.visibility = "hidden";
    }else if(state === "loaded"){
      setTimeout(function() {
        self.loadingError.style.visibility = "hidden";
        self.loadingActive.style.visibility = "hidden";
        self.loadingMockup.style.visibility = "hidden";
      }, 100)
      this.loadingMockup.style.opacity = "0%";
      this.mainMenu.style.visibility = "hidden";
      this.resultMenu.style.visibility = "hidden";
      this.allResultsMenu.style.visibility = "hidden";
    }else if(state === "error"){
      this.loadingActive.style.visibility = "hidden";
      this.resultMenu.style.visibility = "hidden";
      this.mainMenu.style.visibility = "hidden";
      this.allResultsMenu.style.visibility = "hidden";
      this.loadingError.style.visibility = "visible";
    }else{
      this.loadingActive.style.visibility = "hidden";
      this.resultMenu.style.visibility = "hidden";
      this.mainMenu.style.visibility = "hidden";
      this.allResultsMenu.style.visibility = "hidden";
      this.loadingError.style.visibility = "visible";
    }
  }

  async setImage(imageObject) {
    if(imageObject.base64String === ''){
      await this.loadImage(`${imageObject.url}`, this.imageView);
    }else{
      await this.loadImage("data:image/png;base64," + imageObject.base64String, this.imageView);;
    }
  }

  async loadImage(data, elem) {
    return new Promise((resolve, reject) => {
      elem.src = data;
      elem.onload = () => {
        resolve(elem)
      }
      elem.onerror = e => {
        reject(e)
      }
    });
  }

  setMirrorView(mirror){
    if(mirror){
      this.imageView.style.transform = "scaleX(-1)"
    }else{
      this.imageView.style.transform = "scaleX(1)"
    }
  }

  appendToResults(id, timeSpent, solved, mirror) {
    if(this.resList) {
      this.resList.insertAdjacentHTML('beforeend', this.templateCreator.getResultCard(id, timeSpent, solved, mirror));
      this.resList.scrollTo(0, this.resList.scrollHeight);
    } else {
      console.log('ViewEngine: Container not defined')
    }
  }

  appendProfileToResults(name, age, note) {
    if(this.resList) {
      this.resList.insertAdjacentHTML('afterbegin', this.templateCreator.getProfileCard(name, age, note));
      this.resList.scrollTo(0, this.resList.scrollHeight);
    } else {
      console.log('ViewEngine: Container not defined')
    }
  }

  clearResults(){
    if(this.resList){
      this.resList.innerHTML = "";
    }else {
      console.log('ViewEngine: Container not defined')
    }
  }

  clearProfiles(){
    if(this.profileList){
      this.profileList.innerHTML = "";
    }else {
      console.log('ViewEngine: Container not defined')
    }
  }

  closeStartForm(){
    [...document.querySelectorAll('.image-view__loading-input')].forEach(item => {
      item.remove();
    })
  }

  openStartForm(){
    this.closeStartForm();
    this.loadingMockup.insertAdjacentHTML('beforeend', this.templateCreator.getNameInput());
    return new Promise((resolve, reject) => {
      let form = document.querySelector('.image-view__loading-input');
      let res = {
        name: "",
        age: 0,
        info: "",
        file: null,
        testType: 0,
        testArray: []
      };

      function handleForm(event) { 
        event.preventDefault(); 
        res.name = document.getElementById("name").value;
        res.age = document.getElementById("age").value;
        res.info = document.getElementById("info").value;
        res.file = document.getElementById("file").files[0];
        if(res.file != null){
           var fr=new FileReader();
            fr.onload=function(){
                const str = fr.result;
                if(str.length <= 3){
                  console.log("Random test")
                }else{
                  const arr = str.split(' ')
                  if(arr.length <= 1){
                    console.log("Rand test")
                  }else{
                    arr.every(item => {
                      let num = parseInt(item)
                      if (isNaN(num)) { 
                        res.testArray = [];
                        res.testType = 0;
                        console.log("File error, random test");
                        return false;
                      }
                      res.testType = 1;
                      res.testArray.push(num)
                      return true;
                    })
                  }
                }

                [...document.querySelectorAll('.image-view__loading-input')].forEach(item => {
                  item.remove();
                })
                resolve(res);
            }
              
            fr.readAsText(res.file);
          }else{
            [...document.querySelectorAll('.image-view__loading-input')].forEach(item => {
              item.remove();
            })
            resolve(res);
          }
      } 
      form.addEventListener('submit', handleForm);
      document.querySelector('.results-item__form-submit-button').addEventListener('click', (e) => {
        
      })

      document.querySelector('.results-item__form-cancel-button').addEventListener('click', (e) => {
        this.closeStartForm();
        reject("canceled");
      })
    });
  }


  openMenu(){
    this.clearResults();
    this.resList.classList.remove("results--summary");
    this.resList.classList.add("results--menu");
    this.setLoading('mainMenu');
  }

  openAllResults(profiles){
    this.clearResults();
    this.clearProfiles();
    profiles.forEach(item =>{
      this.profileList.insertAdjacentHTML('beforeend', this.templateCreator.getResultProfile(item.name));
    })
    document.querySelectorAll(".results-list-menu__profiles-item").forEach(item =>{
      item.addEventListener('click', (e) => {
        e = e || window.event;
        var targ = e.target || e.srcElement || e;
        profiles.forEach(item => {
          if(item.name === targ.innerHTML){
            this.currentResult = item;
            item.results.forEach(res => {
              this.appendToResults(res.imageNum, res.timeSpent, res.solved, res.rightHand);
            })
          }
        })
        this.openResults(targ.innerHTML, true)
      })
    });
    this.resList.classList.remove("results--summary");
    this.resList.classList.add("results--menu");
    this.setLoading('allResultsMenu');
  }

  updateResults(){

  }

  startTesting(name, age, note){
    this.clearResults();
    this.resList.classList.remove("results--menu");
    this.resList.classList.remove("results--summary");
    this.appendProfileToResults(name, age, note)
    this.setLoading('loaded');
  }

  openResults(name, fromMemory){
    if(fromMemory){
      this.appendProfileToResults(this.currentResult.name, this.currentResult.age, this.currentResult.note)
    }

    this.resList.classList.remove("results--menu");
    this.resList.classList.add("results--summary");
    document.querySelectorAll(".results-item").forEach(item => {
      item.classList.add("results-item--summary");
    })
    this.setLoading('resultMenu');
  }
};