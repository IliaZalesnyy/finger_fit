export class Image {
	id = 0;
	isFavorite = false;
	blob = undefined;
	url = "null";
	width = 0;
	height = 0;
	mirror = false;
	base64String = "";

	constructor(id, blob, url, mirror){
		this.id = id;
		this.blob = blob;
		this.url = url;
		this.mirror = mirror;
	}

	fromStorage(obj){
		this.id = obj.id;
		this.isFavorite = obj.isFavorite;
		this.blob = obj.blob;
		this.url = obj.url;
		this.width = obj.width;
		this.mirror = obj.mirror;
		this.height = obj.height;
		this.base64String = obj.base64String;
	}
}