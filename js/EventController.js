export class EventController {
  constructor(events) {
    this.events = events;
    this.init();
  }

  init() {
    this.events.forEach((eventData) => {
      const elementsLinks = document.querySelectorAll(eventData.selector);
      elementsLinks.forEach(item => {
        item.addEventListener('click', () => {
          eventData.callbacks.forEach((callback) => {
            callback();
          });
        });
      })
    })
  }

  on(event, callback) {
    const element = this.events.find((eventData) => {
      return eventData.eventName === event;
    });

    if(element) {
      element.callbacks.push(callback);
    }else{
      console.log('event not found');
    }
  }
}