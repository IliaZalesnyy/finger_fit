import {ImageProvider} from '/js/ImageProvider.js';
import {EventController} from '/js/EventController.js';
import {ViewEngine} from '/js/ViewEngine.js'
import {AnimationProvider} from '/js/AnimationProvider.js'
import {Stopwatch} from '/js/Stopwatch.js'

let imageProvider = new ImageProvider();

( () => {
  const events = [
    {
      eventName: 'try-again',
      selector: '.image-view__loading-tryagain',
      callbacks: []
    },
    {
      eventName: 'cancel',
      selector: '.image-view__loading-cancel',
      callbacks: []
    },
    {
      eventName: 'confirm',
      selector: '.image-view__content-confirm',
      callbacks: []
    },
    {
      eventName: 'refuse',
      selector: '.image-view__content-refuse',
      callbacks: []
    },
    {
      eventName: 'stop',
      selector: '.navigation__stop-button',
      callbacks: []
    },
    {
      eventName: 'start',
      selector: '.main-menu__start-button',
      callbacks: []
    },
    {
      eventName: 'copyResults',
      selector: '.results-item__copy-button',
      callbacks: []
    },
    {
      eventName: 'mainMenu',
      selector: '.results-item__restart-button',
      callbacks: []
    },
    {
      eventName: 'allResults',
      selector: '.main-menu__results-button',
      callbacks: []
    }

  ];

  const controller = new EventController(events);
  const viewEngine = new ViewEngine();
  const animationProvider = new AnimationProvider();


  let stopwatch = new Stopwatch();  

  let isLoading = true;

  openMenu();

  controller.on("try-again", function() {
    nextImage()
  });

  controller.on("cancel", function() {
    viewEngine.setLoading("loaded")
  });

  controller.on("confirm", function(){
      nextImage(true);
  });

  controller.on("refuse", function() {
      nextImage(false);
  })

  controller.on("stop", function() {
      stopTesting(false);
  })

  controller.on("start", function() {
      startTesting();
  })

  controller.on("mainMenu", function() {
      openMenu();
  })

  controller.on("copyResults", function() {
      copyCurrentResults();
  })

  controller.on("allResults", function() {
      openAllResults();
  })

  function openMenu() {
    viewEngine.openMenu();
  }

  function openAllResults() {
    imageProvider.loadResultList()
    .then(res => {
      viewEngine.openAllResults(res);
    })
  }

  function startTesting(){
    viewEngine.openStartForm().then(res => {
      imageProvider.startTesting(res.name, res.age, res.info, res.testType, res.testArray);
      viewEngine.startTesting(res.name, res.age, res.info);
      nextImage(true);
    }).catch(rej => {
      console.log('canceled');
    })
  }

  function nextImage(solved){
    isLoading = true;
    viewEngine.setLoading("loading");
    let timeSpent = stopwatch.getTime();
    if(imageProvider.currentIndex >= 0){
      imageProvider.setCurrentSolved(timeSpent, solved);
      viewEngine.appendToResults(imageProvider.testingArray[imageProvider.currentIndex].id, timeSpent, solved, imageProvider.testingArray[imageProvider.currentIndex].mirror);
    }
    stopwatch.stop();
    stopwatch.reset();
    imageProvider.getNextImage()
      .then(imageObject => {
        if(imageObject.url == "null"){   
          viewEngine.setLoading("error");
        }else{
          stopwatch.start();
          animationProvider.imageOutLeft('.image-view__content-img');
          viewEngine.setMirrorView(imageObject.mirror);
          viewEngine.setImage(imageObject);
          setTimeout(function() {
            animationProvider.imageInFromRight('.image-view__content-img')
          }, 110)
          viewEngine.setLoading("loaded");
        }
        isLoading = false;
      })
      .catch(err => {
        stopTesting(solved);
      });
  }

  function stopTesting(solved) {
    let timeSpent = stopwatch.getTime();
    // if(!solved){
    //   imageProvider.setCurrentSolved(timeSpent, solved);
    //   viewEngine.appendToResults(imageProvider.testingArray[imageProvider.currentIndex].id, timeSpent, solved, imageProvider.testingArray[imageProvider.currentIndex].mirror);
    // }
    stopwatch.stop();
    stopwatch.reset();
    viewEngine.openResults(imageProvider.currentTestingName);
    imageProvider.endTesting().then(res => {
      viewEngine.currentResult = res;
    });
  }

  function copyCurrentResults() {

    
    navigator.clipboard.writeText(jsonToCsv(viewEngine.currentResult))
    .then(() => {
        console.log('Result copied');
    })
    .catch(err => {
        console.error('Error in copying text: ', err);
    });
  }

  function jsonToCsv(input) {
    const items = input.results;
    const replacer = (key, value) => value === null ? '' : value // specify how you want to handle null values here
    let header = Object.keys(items[0]);
    header.pop()
    header.splice(1, 0, "rightHand");
    const csv = [
      ["Номер изображения", "Рука", "Затраченное время", "Получилось"].join(';'), // header row first
      ...items.map(row => header.map(fieldName => {
        if(fieldName === "rightHand"){
          return ((row[fieldName])? "Правая" : "Левая");
        }else{
          return JSON.stringify(row[fieldName], replacer);
        }
      }).join(';'))
    ].join('\r\n')

    return csv;
  }
})();