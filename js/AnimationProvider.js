export class AnimationProvider {
  constructor(){
  }

  imageOutLeft(selector) {
    document.querySelector(selector).animate([
        {
          transform: "translate(0%, 0%)",
          opacity: '100%'
        },
        {
          transform: "translate(-500px, 0%)",
          opacity: '0%'
        }
      ], 120)
  }

  imageInFromRight(selector) {
    document.querySelector(selector).animate([
        {
          transform: "translate(500px, 0%)",
          opacity: '0%'
        },
        {
          transform: "translate(0%, 0%)",
          opacity: '100%'
        }
      ], 120)
  }

  imageOutRight(selector) {
    document.querySelector(selector).animate([
        {
          transform: "translate(0%, 0%)",
          opacity: '100%'
        },
        {
          transform: "translate(500px, 0%)",
          opacity: '0%'
        }
      ], 120)
  }

  imageInFromLeft(selector) {
    document.querySelector(selector).animate([
        {
          transform: "translate(-500px, 0%)",
          opacity: '0%'
        },
        {
          transform: "translate(0%, 0%)",
          opacity: '100%'
        }
      ], 120)
  }

  imageFadeIn(selector) {
    document.querySelector(selector).animate([
        {
          opacity: '0%'
        },
        {
          opacity: '100%'
        }
      ], 120)
  }

  imageFadeOut(selector) {
    document.querySelector(selector).animate([
        {
          opacity: '100%'
        },
        {
          opacity: '0%'
        }
      ], 120)
  }

  easeOutFavorite(favImg) {
    if(favImg != null && favImg != undefined){
      favImg.animate([
          {
            visibility: 'hidden',
            aspectRatio: '1.22'
          },
          {
            visibility: 'hidden',
            aspectRatio: '5'
          }
        ], 100)
    }
  }
}