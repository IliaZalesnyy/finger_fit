export class Profile {
	name = "";
	age = "";
	note = "";
	results = [];

	constructor(name, age, note){
		this.name = name;
		this.age = age;
		this.note = note
	}

	fromStorage(obj){
		this.name = obj.name;
		this.age = obj.age;
		this.note = obj.note
		this.results = obj.results;
	}
}