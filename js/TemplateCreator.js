export class TemplateCreator {
  getResultCard(id, timeSpent, solved, mirror) {
    const template = `
        <div id="${"res_id_" + id}" class="results-item">
          <div class="results-item__img results-item__img--${(solved)? "solved" : "skipped"} ${(mirror)? "result--mirror" : ""}" style="background-image: url('/img/fingers/lvl1/${id}.png');"></div>
          <div class="results-item__time-spent"> №${id} ${(mirror)? "Правая":"Левая"} 
            Time: ${timeSpent}s</div>
        </div>
    `;
    return template;
  }

  getProfileCard(name, age, note) {
    const template = `
        <div id="${"prof_name_" + name}" class="results-item">
          <div class="results-item__img" style="background-image: none;">
          </div>
            <div class="results-item__time-spent" style="text-align: left;"> ${name} ${age}лет ${note}</div>  
        </div>
    `;
    return template;
  }
  
  getComment(text){
    const template = `
      <div class="comments-container__list-comment">${text}</div>
    `;
    return template;
  }

  getResultProfile(name){
    const template = `
      <div class="results-list-menu__profiles-item">${name}</div>
    `;
    return template;
  }

  getNameInput() {
    const template = `
        <form class="image-view__loading-input">
            <p class="image-view__loading-text-loading">Enter info</p>
            <input class="image-view__loading-input__field" type="text" id="name" name="name" placeholder="Имя" required minlength="3" maxlength="10" size="10">
            <input class="image-view__loading-input__field" type="number" id="age" name="age" placeholder="Возраст" required maxlength="2" size="10">
            <input class="image-view__loading-input__field" type="text" id="info" name="info" placeholder="Доп. инфо" required minlength="4" maxlength="50" size="10">
            <div class="image-view__loading-input__field">
              <p style="color: white; font-size: 14px">Выберите файл теста (номера фото через пробел, отобразятся по очереди левый и правый)</p>
              <br>
              <p style="color: white; font-size: 14px">Если не выбрать файл, то тест будет из случайных фото</p>
              <br>
              <input style="color: white" type="file" id="file" accept="text/plain">
            </div>
            <div class="image-view__loading-input__buttons">
              <button class="results-item__form-cancel-button"></button>
              <button class="results-item__form-submit-button" type="submit"></button>
            </div>
          </form>
    `;
    return template;
  }
}