export class Result {
	imageNum = 0;
	timeSpent = 0;
	solved = false;
	rightHand = false;

	constructor(imageNum, timeSpent, solved, rightHand){
		this.imageNum = imageNum;
		this.timeSpent = timeSpent;
		this.solved = solved;
		this.rightHand = rightHand;
	}

	fromStorage(obj){
		this.imageNum = obj.imageNum;
		this.timeSpent = obj.timeSpent;
		this.solved = obj.solved;
		this.rightHand = obj.rightHand;
	}
}