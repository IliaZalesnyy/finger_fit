export class Stopwatch {
  constructor(){
    this.clock = 0;
    this.reset();
  }

  start() {
    if (!this.interval) {
      this.offset = Date.now();
      this.interval = setInterval(() => {
        this.clock += this.delta();
      }, 1);
    }
  }

  stop() {
    if (this.interval) {
      window.clearInterval(this.interval);
      this.interval = null;
    }
  }

  reset() {
    this.clock = 0;
    this.getTime();
  }

  delta() {
    var now = Date.now();
    var d = now - this.offset;

    this.offset = now;
    return d;
  }

  getTime() {
    return this.clock / 1000;
  }
};